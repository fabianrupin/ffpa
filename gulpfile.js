'use strict';

var gulp = require('gulp');
var less = require('gulp-less');
var cssnano = require('gulp-cssnano');
var sourcemaps = require('gulp-sourcemaps');
var autoprefixer = require('gulp-autoprefixer');

gulp.task('workflow', function(){
	gulp.src('./src/less/**/*.scss')

	.pipe(gulp.dest('../dist/css/'))
});
